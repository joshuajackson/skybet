<?php

namespace App\Controllers;

class Main {

    public function main() {
        
        \App\Models\CSRFToken::register();
        
        \App\Models\Record::loadAll();
        // send to form view
        include('app/views/form.php');
    }

    public function add() {
        $record = new \App\Models\Record;
        $record->setFirstName(preg_replace("/[^A-Za-z0-9 ]/", '', $_POST['firstname']));
        $record->setLastName(preg_replace("/[^A-Za-z0-9 ]/", '', $_POST['lastname']));
        
        if(!empty($_POST['key'])) {
            $record->setFileName(preg_replace("/[^A-Za-z0-9 ]/", '', $_POST['key']));
        }
        
        if($record->save()) {
            header("HTTP/1.1 201 Created");
        } else {
            header("HTTP/1.1 400 Bad Request");
        }
    }
    
    public function delete() {
        $key = preg_replace("/[^A-Za-z0-9 ]/", '', $_POST['key']);
        if(\App\Models\Record::delete($key)) {
            header('HTTP/1.1 200 OK');
        } else {
            header('HTTP/1.1 400 Bad Request');
        }
        
    }
}
