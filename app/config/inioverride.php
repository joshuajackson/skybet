<?php

ini_set('display_errors',1);
ini_set( 'date.timezone', 'Europe/London' );
ini_set("log_errors", 1);
ini_set("error_log", constant(APP_MODE.'_LOGS').'/'.date('Ymd').'error.log');
