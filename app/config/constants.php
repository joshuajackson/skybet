<?php

// define app mode // LIVE OR TEST
define('APP_MODE','TEST');


////////////-LIVE CONSTANTS-///////////////

// define file store
define('LIVE_FILE_STORE','app/files/live');

// define default controller
define('LIVE_DEFAULT_CONTROLLER','\App\Controllers\Main');

// define default method
define('LIVE_DEFAULT_METHOD','main');

// define logs directory
define('LIVE_LOGS','logs/live');


////////////-TEST CONSTANTS-///////////////

// define test file store
define('TEST_FILE_STORE','app/files/test');

// define default controller
define('TEST_DEFAULT_CONTROLLER','\App\Controllers\Main');

// define default method
define('TEST_DEFAULT_METHOD','main');

// define logs directory
define('TEST_LOGS','logs/test');
