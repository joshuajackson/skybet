<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <title>SkyBet Tech Test</title>
</head>
<body>
    <div class="container-fluid mt-5" id="main-container">
        <div class="row justify-content-center">    
            <form id="main" class="form">
                <input type="hidden" name="csrfname" value="<?=$_SESSION['csrf_name'];?>">
                <input type="hidden" name="csrftoken" value="<?=$_SESSION['csrf_token'];?>">
                <input type="hidden" name="key" value="">
                <div class="form-group row">
                    <label class="" for="firstname">First name</label>
                    <input type="text" class="form-control" name="firstname" placeholder="first name" maxlength="50" title="Insert first name here" required>
                </div>
                <div class="form-group row">
                    <label class="" for="lastname">Last name</label>
                    <input type="text" class="form-control" name="lastname" placeholder="last name" maxlength="50" title="Insert last name here" required>
                </div>
                <button type="button" id="add" class="btn btn-info float-right ml-1">Add</button>
                <button type="button" id="cancel" class="btn btn-outline-info float-right">Cancel</button>
            </form>
        </div>
        <div class="row justify-content-center mt-5">
            <table id="table" class="table table-striped table-hover table-sm mt-5" style="text-align:center">
                <thead> 
                    <tr>
                        <th scope="col">First name</th>
                        <th scope="col">Last name</th>
                        <th></th>
                    </tr>
                </thead>
                <tfoot> 
                    <tr>
                        <td scope="col">First name</td>
                        <td scope="col">Last name</td>
                        <td></td>
                    </tr>
                </tfoot>
                <tbody id="list-body"> 
                    <?php foreach($_SESSION['records'] as $key=>$record): ?>
                    <tr data-key="<?=$key;?>">
                        <td><?=$record['firstname'];?></td>
                        <td><?=$record['lastname'];?></td>
                        <td>
                            <button type="button" data-key="<?=$key;?>" class="btn btn-secondary edit">edit</button>
                            <button type="button" data-key="<?=$key;?>" class="btn btn-primary delete" id="delete-<?=$key;?>">delete</button>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>   
            </table>
        </div>
    </div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script>
$('.edit').click(function () {
    $('#add').html('update')
    data_key = $(this).attr('data-key');
    $('input[name="key"]').val(data_key);
    row = $(this).parent().parent();
    $('input[name="firstname"]').val(row.children('td:first').text());
    $('input[name="lastname"]').val(row.children('td:nth-child(2)').text());
});

$('#cancel').click(function() {
    $('input[name="firstname"]').val('');
    $('input[name="lastname"]').val('');
    $('input[name="key"]').val('');
});

$('.delete').click(function() {
    data_key = $(this).attr('data-key');
    $.ajax({
        url: '/main/delete',
        type: 'post',
        data: {'key':data_key},
        statusCode: {
            400: function() {
                alert('record could not be deleted');
            },
            200: function() {
                $('tr[data-key="'+key+'"]').remove();
            }
        }
    });
});

function addNew() {
    firstname = $('input[name="firstname"]').val();
    lastname = $('input[name="lastname"]').val();
    key = $('input[name="key"]').val();
    
    $.ajax({
        url: '/main/add',
        type: 'post',
        data: {'firstname': firstname,'lastname': lastname,'key': key},
        complete: function(result) {
            console.log(result);
        },
        statusCode: {
            400: function() {
                alert( "record could not be added" );
            },
            201: function() {
                if(key == '') {
                    addItem();
                } else {
                    $('#add').html('save');
                    row = $('tr[data-key="'+key+'"]');
                    row.children('td:first').text(firstname);
                    row.children('td:nth-child(2)').text(lastname);
                    $('input[name="firstname"]').val('');
                    $('input[name="lastname"]').val('');
                    $('input[name="key"]').val('');
                }
            }
        }
    });
}

function addItem() {
    $('#list-body').append('<tr><td>'+$('input[name="firstname"]').val()+'</td><td>'+$('input[name="lastname"]').val()+'</td></tr>');
    $('input').val('');
}

$('#add').click(function() {
    addNew();
});
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>$(function() { $('#table').DataTable({'ordering':true});
$('input[type="search"]').addClass('form-control');
$('select[name="table_length"]').addClass('form-control');
 })</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
