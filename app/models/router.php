<?php

namespace App\Models;

class Router {
    
    private $requested_controller;
    
    private $requested_method;
    
    private function __construct() {
        $this->parseUrl();
        $this->dispatch();
    }
    
    private function parseUrl() {
        // get uri path (uri minus query string) 
        $uri = strtok($_SERVER["REQUEST_URI"],'?');
       
        // clean
        $uri = filter_var($uri, FILTER_SANITIZE_URL);
        
        // trim slashes
        $uri = trim($uri,'/');
        
        // split
        $uri_parts = explode('/',$uri);

        $this->requested_controller = '\App\Controllers\\'.$uri_parts[0];
        
        if(isset($uri_parts[1])) {
            $this->requested_method = $uri_parts[1];
        }
        
        if(empty($this->requested_controller) || !self::controllerExists($this->requested_controller)) {
            // no uri specified, load default controller
            $this->requested_controller = constant(APP_MODE.'_DEFAULT_CONTROLLER');
            $this->requested_method = constant(APP_MODE.'_DEFAULT_METHOD');
        } else {
            // valid controller given
            // check if requested method is given and exists
            if(empty($this->requested_method) || !method_exists($this->requested_controller,$this->requested_method)) {
                // method does not exist, set to default
                $this->requested_method = constant(APP_MODE.'_DEFAULT_METHOD');
            }
        }        
    }
    
    private static function controllerExists($controller) {
        return class_exists($controller);
    }
    
    
    private function dispatch() {
        $handler = new $this->requested_controller();
        $handler->{$this->requested_method}();
        exit;
    }
    
    
    public static function load() {
        $route = new self();
    }
}
