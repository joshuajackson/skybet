<?php

namespace App\Models;

class Record {
    
    private $filename;
    
    private $firstname;
    
    private $lastname;
    
    public function getFileName() {
        return $this->filename;
    }
    
    public function getFirstName() {
        return $this->firstname;
    }
    
    public function getLastName() {
        return $this->lastname;
    }
    
    public function setFileName($filename) {
        $this->filename = $filename;
    }

    public function setFirstName($firstname) {
        $this->firstname = $firstname;
    }
    
    public function setLastName($lastname) {
        $this->lastname = $lastname;
    }
    
    public function save() {
        $string = json_encode(['firstname'=>$this->firstname, 'lastname'=>$this->lastname]);
        
        if(empty($this->filename)) {
            do {
                $this->filename = bin2hex(random_bytes(10));
                $filename = constant(APP_MODE.'_FILE_STORE').'/'. $this->filename;
            } while (file_exists($filename));
        } else {
            $filename = constant(APP_MODE.'_FILE_STORE').'/'.$this->filename;
        }
        
        $fh = fopen($filename,'wb');
        fwrite($fh,$string);
        fclose($fh);
        chmod($filename, 0755);
        
        $_SESSION['records'][$this->filename] = ['firstname'=>$this->firstname, 'lastname'=>$this->lastname];
        
        return file_exists($filename);
    }
    
    public static function loadAll() {
        $dir = constant(APP_MODE . '_FILE_STORE');
        $_SESSION['records'] = [];
        
        foreach(scandir($dir) as $file) {
            if(!is_dir($file)) {
                $_SESSION['records'][$file] = json_decode(file_get_contents($dir.'/'.$file),true);
            }
        }
    }
    
    public static function delete($filename) {
        $dir = constant(APP_MODE.'_FILE_STORE');
        
        unlink($dir.'/'.$filename);
        unset($_SESSION['records'][$filename]);
        
        return !file_exists($dir.'/'.$filename);
    }

}
