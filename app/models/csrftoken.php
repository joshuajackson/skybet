<?php

namespace App\Models;

class CSRFToken {
    
    private $file_name;
    
    private $token;
    
    public function setFileName($name) {
        $this->file_name = $name;
    }
    
    public function setToken($token) {
        $this->token = $token;
    }
    
    public function getToken() {
        return $this->token;
    }
    
    public function getFileName() {
        return $this->file_name;
    }
    
    private function __construct() {}
    
    /**
     * Creates new token
     *
     */
    public static function register() {
        
        $csrf = new self();
        
        // create file name
        $csrf->setFileName(bin2hex(random_bytes(50)));
        
        // create csrf token
        $csrf->setToken(bin2hex(random_bytes(100)));
        
        $_SESSION['csrf_token'] = $csrf->getToken();
        $_SESSION['csrf_name'] = $csrf->getFileName();
        
        $csrf->save();
    
    }   
    
    /**
     * saves token to file
     */
    private function save() {
        // create file
        $fh = fopen('app/csrf/'.$this->file_name,'wb');
        fwrite($fh,$this->token);   
        fclose($fh);
        chmod('app/csrf/'.$this->file_name,0755);
    }
    
    /**
     * verifies if token is correct
     */
    public static function verify($file_name, $token) {
        
        $file_name = preg_replace("/[^A-Za-z0-9 ]/", '',$file_name);
        $token = preg_replace("/[^A-Za-z0-9 ]/", '', $token);
        
        if(file_exists('app/csrf/'.$file_name)) {
            $real_token = file_get_contents('app/csrf/'.$file_name);
            if($real_token === $token) {
                unlink('app/csrf/'.$file_name);
                return true;
            }
        }   
        return false;
    } 
    
}
