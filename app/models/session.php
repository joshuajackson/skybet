<?php

namespace App\Models;

class Session {
    
    private function __construct() {
        
        // start session if not already    
        if(session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
            $this->createSessionCache();
        }       
    }
    
    private function createSessionCache() {
        $_SESSION['APP'] = [];
        $_SESSION['APP']['MODE'] = APP_MODE;
        $_SESSION['APP']['DEFAULT_CONTROLLER'] = constant(APP_MODE.'_DEFAULT_CONTROLLER');
        $_SESSION['APP']['DEFAULT_METHOD'] = constant(APP_MODE.'_DEFAULT_METHOD');
    }

    public static function new() {
        $session = new self();
    }
}
