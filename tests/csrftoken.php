<?php

declare(strict_types=1);

include "app/config/bootstrap.php";

use PHPUnit\Framework\TestCase;
use App\Models\CSRFToken;

final class CSRFTokenTest extends TestCase
{
    public function testRegisterCreatesToken() {
        CSRFToken::Register();
        $this->assertTrue(!empty([$_SESSION['csrf_token'],$_SESSION['csrf_name']]));
    }
    
    public function testValidTokenReturnsTrue() {
        CSRFToken::Register();
        $this->assertTrue(CSRFToken::verify($_SESSION['csrf_name'],$_SESSION['csrf_token']),print_r($_SESSION,true));
    }
    
    public function testInvalidTokenReturnsFalse() {
        $this->assertNotTrue(CSRFToken::verify('invalid','invalid'));
    }
}
