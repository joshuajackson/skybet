<?php

declare(strict_types=1);

include 'app/config/bootstrap.php';

use PHPUnit\Framework\TestCase;

final class RecordTest extends TestCase
{
    public function testConstructorWorks() {
        $this->assertTrue(self::helper1());
    }
    
    public function testDeleteWorks() {
        
        $record = new \App\Models\Record();
        $record->setFirstName('test');
        $record->setLastName('test');
        $record->save();
        $this->assertTrue(\App\Models\Record::delete($record->getFileName()));
        
    }
    
    public static function helper1 () {
        $record = new \App\Models\Record();
        $record->setFirstName('test');
        $record->setLastName('test');
        return ($record->save());
    }
}
